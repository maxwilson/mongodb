db.medico.find()
db.paciente.find()
db.prontuario.find()

db.getCollection('prontuario').find({})


db.prontuario.aggregate(
    { $project : { Titulo : 1 , medicamento : 1 }},
    { $unwind : "$medicamento" } 
);

db.medico.aggregate(
    { $project : { Nome : 1 , Especialidade : 1 }},
    { $unwind : "$Especialidade" } 
);
    
db.paciente.aggregate(
    { $project : { Nome : 1 , Telefone: 1, CPF : 1 }},
    { $unwind : "$CPF" } 
);
